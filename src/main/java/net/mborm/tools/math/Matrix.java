package net.mborm.tools.math;

import java.util.Arrays;

/**
 * Class for structured matrices
 *
 * @author Maximilian Borm
 * @version 1.1-stable
 **/

public class Matrix {

    /**
     * components of the matrix
     */
    protected double[][] components;

    public Matrix() {
    }

    /**
     * Constructor for matrix with n vectors
     * @param arr double[][] array for matrix
     */
    public Matrix(double[][] arr) {
        this.components = arr;
    }

    /**
     * Get the matrix object
     * @return this
     */
    public Matrix getMatrix() {
        return this;
    }

    /**
     * Dimension of the matrix (mxn)
     * @return int array {m, n}
     */
    public int[] getDim() {
        return new int[] {this.components.length, this.components[0].length};
    }

    /**
     * Returns matrix as double array
     * @return double[][] array
     */
    public double[][] getArray() {
        return this.components;
    }

    /**
     * {@inheritDoc}
     * Overrides the standard toString() function to structurize the matrix output
     * @return structurized matrix String
     */
    @Override
    public String toString() {
        String tmp = "";

        for(double[] arr : this.components)
            tmp += Arrays.toString(arr) + "\n";

        return tmp;
    }

    /**
     * Prints the overwritten toString() function
     */
    public void printMatrix() {
        System.out.println(this.toString());
    }
}
