package net.mborm.tests.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import java.util.Arrays;

import net.mborm.tools.math.Matrix;

/**
  * Tests for Matrix class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/

public class MatrixTest {
    @Test
    public void testConstructor() {
        double[][] t = new double[][]{{1,2},{1,3},{1,3}};
        Matrix a = new Matrix(t);

        assertTrue(Arrays.deepEquals(t, a.getArray()));
    }

    @Test
    public void testgetMatrix() {
        Matrix a = new Matrix(new double[][]{{1,3},{1,2}});

        assertTrue(a == a.getMatrix());
    }

    @Test
    public void testgetDim() {
        Matrix a = new Matrix(new double[][]{{1,2},{1,2},{1,2}});
        int[] dim = a.getDim();

        assertEquals(3, dim[0]);
        assertEquals(2, dim[1]);
    }
        
}
