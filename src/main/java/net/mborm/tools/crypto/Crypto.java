package net.mborm.tools.crypto;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Encryption and decryption functions 
 *
 * @author Maximilian Borm
 * @version 1.2-testing
 **/

public class Crypto {

    /**
     * En- or decrypt a given string with the caesar chiffre
     *
     * @param   t   String to be en- / decrypted
     * @return decrypted or encrypted String
     */
    public static String Caesar(String t) {
        String c = "";

        for(int i = 0; i < t.length(); i++) {
            int x = t.charAt(i);

            if(x >= 'a' && x <= 'z')
                x = (x - 'a' + 13) % 26 + 'a';

            else if(x >= 'A' && x <= 'Z')
                x = (x - 'A' + 13) % 26 + 'A';

            c += (char) x;
        }

        return c;
    }


    /**
     * Generating a secure password with a length of 8 chars
     * TODO: add variable length of password
     *
     * @return String password with length of 8 chars
     */
    public static String generateSecurePassword() {
        StringBuilder builder = new StringBuilder();
        final String[] specialChars = new String[]{"@", "!", "$", "%", "&", "/", "=", "?", "#", "."};
        final String[] bigchars = "ABCDEFGHJKLMNOPQRSTUVWXYZ".split("");
        final String[] chars = "abcdefghijkmnopqrstuvwxyz".split("");

        for(int i = 0; i < 2; i++) {
            builder.append(specialChars[ThreadLocalRandom.current().nextInt(0, specialChars.length)]);
            builder.append(bigchars[ThreadLocalRandom.current().nextInt(0, bigchars.length)]);
            builder.append(chars[ThreadLocalRandom.current().nextInt(0, chars.length)]);
            builder.append(String.valueOf(ThreadLocalRandom.current().nextInt(0, 9 + 1)));
        }

        String[] finalPass = builder.toString().split("");
        Collections.shuffle(Arrays.asList(finalPass));

        return Arrays.toString(finalPass).replaceAll("\\[|\\]|\\,|\\s", "");
    }
}
