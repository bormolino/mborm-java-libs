package net.mborm.tools.sort;

/**
 * Selection sort algorithm with generics
 *
 * @author Maximilian Borm
 * @version 1.0-stable
 **/

@SuppressWarnings("unchecked")
public class Selectionsort<T extends Comparable<? super T>> {

    /**
     * Sorting method
     *
     * @param array array to be sorted
     * @return sorted array
     */
    public T[] sort(T... array) {
        int left = 0;

        while (left < array.length) {
            int min = left;

            for (int i = left + 1; i < array.length; i++)
                if (array[i].compareTo(array[min]) <= 0)
                    min = i;

            T tmp = array[min];
            array[min] = array[left];
            array[left] = tmp;
            left++;
        }

        return array;
    }
}
