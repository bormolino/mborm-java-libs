# mborm-java-libs

Collection of miscellaneous java functions and methods. You need maven to compile the project into a jar.


### Compile the code

Only run:
```
mvn clean package
```


### Unit-tests

The output should look like this:
```
Results :

Tests run: 67, Failures: 0, Errors: 0, Skipped: 0
```


## Authors

* **Maximilian Borm** - *Developer* - [@bormolino](https://gitlab.com/bormolino)



## License

See the [LICENSE](LICENSE) file for details



