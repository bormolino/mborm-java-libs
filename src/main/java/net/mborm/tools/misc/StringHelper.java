package net.mborm.tools.misc;

import java.util.regex.Pattern;

/**
 * Class with helpful string functions
 *
 * @author Maximilian Borm
 * @version 1.0-stable
 **/

public final class StringHelper {

    /**
     * Checks if a given String is a palindrome
     *
     * @param s String to be checked
     * @return true or false
     */
    public static boolean isPalindrome(String s) {
        return new StringBuffer(s).reverse().toString().equals(s);
    }

    /**
     * Checks a String with a given regex
     *
     * @param s       String to be checked
     * @param pattern regex pattern
     * @return true or false
     */
    public static boolean isMatch(String s, String pattern) {
        try {
            return Pattern.compile(pattern).matcher(s).matches();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }
}
