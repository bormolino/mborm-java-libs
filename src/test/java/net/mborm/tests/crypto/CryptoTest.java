package net.mborm.tests.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import net.mborm.tools.crypto.Crypto;

/**
  * Tests for Crypto class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/

public class CryptoTest {
    @Test
    public void testCaesar() {
        String enc = Crypto.Caesar("HalloTest");
        String dec = Crypto.Caesar(enc);

        assertEquals(enc, "UnyybGrfg");
        assertEquals(dec, "HalloTest");
    }

    @Test
    public void testgenerateSecurePassword() {
        String pw = Crypto.generateSecurePassword();

        //min 1 num, min 1 lowercase, min 1 uppercase, min 1 specialchar, no whitespace, min 8 char length
        assertTrue(pw.matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@!#$%&/=?.])(?=\\S+$).{8,}"));
    }
}
