package net.mborm.tools.math;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class for calculating the runtime of a programm
 *
 * @author Maximilian Borm
 * @version 1.0-stable
 **/

public final class CalcTime {
    private static long startTime;

    /**
     * Start time counter
     */
    public static void startCalc() {
        startTime = System.currentTimeMillis();
    }

    /**
     * Stopping counter an getting counted runtime
     *
     * @return String runtime in format "HH:mm:ss:SSS"
     */
    public static String getCalcTime() {
        return (new SimpleDateFormat("HH:mm:ss:SSS")).format(new Date(System.currentTimeMillis() - startTime - 3600000));
    }
}
