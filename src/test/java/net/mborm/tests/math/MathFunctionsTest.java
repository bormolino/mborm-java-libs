package net.mborm.tests.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Arrays;

import net.mborm.tools.math.MathFunctions;
import net.mborm.tools.math.Vector;
import net.mborm.tools.math.Matrix;

/**
  * Tests for MathFunction class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/

public class MathFunctionsTest {
    public static final double DELTA = 0.00000001d;

    @Test
    public void testintToBinary() {
        String bin = MathFunctions.intToBinary(42);
        String bin2 = MathFunctions.intToBinary(0);
        String bin3 = MathFunctions.intToBinary(1);

        assertEquals("101010", bin);
        assertEquals("0", bin2);
        assertEquals("1", bin3);
    }

    @Test
    public void testbinaryToInt() {
        int num = MathFunctions.binaryToInt("101010");
        int num2 = MathFunctions.binaryToInt("0");
        int num3 = MathFunctions.binaryToInt("1");

        assertEquals(42, num);
        assertEquals(0, num2);
        assertEquals(1, num3);
    }

    @Test
    public void testintToHex() {
        String hex = MathFunctions.intToHex(0);
        String hex2 = MathFunctions.intToHex(123456789);

        assertEquals("0", hex);
        assertEquals("75bcd15", hex2);
    }

    @Test
    public void testhexToInt() {
        int num = MathFunctions.hexToInt("0");
        int num2 = MathFunctions.hexToInt("75bcd15");

        assertEquals(0, num);
        assertEquals(123456789, num2);
    }

    @Test
    public void testgetEasternDate() {
        int[] exception1 = MathFunctions.getEasternDate(1981);
        int[] exception2 = MathFunctions.getEasternDate(1954);
        int[] year2018 = MathFunctions.getEasternDate(2018);

        assertArrayEquals(new int[]{19, 4, 1981}, exception1);
        assertArrayEquals(new int[]{18, 4, 1954}, exception2);
        assertArrayEquals(new int[]{1, 4, 2018}, year2018);
    }

    @Test
    public void testbinomial() {
        double num = MathFunctions.binomial(49, 6);
        double num2 = MathFunctions.binomial(5, 3);

        assertEquals(13983816.0d, num, DELTA);
        assertEquals(10.0d, num2, DELTA);
    }

    @Test
    public void testrecFactorial() {
        double num = MathFunctions.recFactorial(0);
        double num2 = MathFunctions.recFactorial(1);
        double num3 = MathFunctions.recFactorial(12);

        assertEquals(1.0d, num, DELTA);
        assertEquals(1.0d, num2, DELTA);
        assertEquals(479001600.0d, num3, DELTA);
    }

    @Test
    public void testfactorial() {
        double num = MathFunctions.factorial(0);
        double num2 = MathFunctions.factorial(1);
        double num3 = MathFunctions.factorial(12);

        assertEquals(1.0d, num, DELTA);
        assertEquals(1.0d, num2, DELTA);
        assertEquals(479001600.0d, num3, DELTA);
    }

    @Test
    public void testgetDigitSum() {
        int num = MathFunctions.getDigitSum(123456789);
        int num2 = MathFunctions.getDigitSum(45);

        assertEquals(45, num);
        assertEquals(9, num2);
    }

    @Test
    public void testGetDigitSum() {
        BigInteger num = new BigInteger("9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999");
        BigInteger res = new BigInteger("1845");
        BigInteger n = MathFunctions.getDigitSum(num);

        assertEquals(res, n);
    }

    @Test
    public void testgetBruttoNetto() {
        double[] res = MathFunctions.getBruttoNetto(15.50, 31, 19);

        assertArrayEquals(new double[]{480.5d, 389.205d}, res, DELTA);
    }

    @Test
    public void testcompoundInterest() {
        double num = MathFunctions.compoundInterest(1000, 5, 50);

        assertEquals(11467.399785753674d, num, DELTA);
    }

    @Test
    public void testgcd() {
        int num = MathFunctions.gcd(12, 18);
        int num2 = MathFunctions.gcd(3528, 3780);

        assertEquals(6, num);
        assertEquals(252, num2);
    }

    @Test
    public void testlcm() {
        int num = MathFunctions.lcm(12, 18);
        int num2 = MathFunctions.lcm(3528, 3780);

        assertEquals(36, num);
        assertEquals(52920, num2);
    }

    @Test
    public void testisPentagonal() {
        boolean a = MathFunctions.isPentagonal(2882);
        boolean b = MathFunctions.isPentagonal(3151);
        boolean c = MathFunctions.isPentagonal(21);
        boolean d = MathFunctions.isPentagonal(-3151);

        assertTrue("2882 is pentagonal", a);
        assertTrue("3151 is pentagonal", b);
        assertFalse("21 is not pentagonal", c);
        assertFalse("negative nums are not defined pentagonal", d);
    }

    @Test
    public void testisPrime() {
        boolean a = MathFunctions.isPrime(-2);
        boolean b = MathFunctions.isPrime(2);
        boolean c = MathFunctions.isPrime(1);
        boolean d = MathFunctions.isPrime(100003619);
        boolean e = MathFunctions.isPrime(997548);

        assertFalse("negative numbers are not defined prime", a);
        assertTrue("2 is prime", b);
        assertFalse("1 is not prime", c);
        assertTrue("100003619 is prime", d);
        assertFalse("997548 is not prime", e);
    }

    @Test
    public void testprimeSum() {
        int sum = MathFunctions.primeSum(1000);

        assertEquals(76127, sum);
    }

    @Test
    public void testisCircularPrime() {
        boolean a = MathFunctions.isCircularPrime(1193);
        boolean b = MathFunctions.isCircularPrime(9311);
        boolean c = MathFunctions.isCircularPrime(933199);
        boolean d = MathFunctions.isCircularPrime(317);
        boolean e = MathFunctions.isCircularPrime(-1193);

        assertTrue("1193 is circular prime", a);
        assertTrue("9311 is circular prime", b);
        assertTrue("933199 is circular prime", c);
        assertFalse("317 is not circular but prime", d);
        assertFalse("-1193 is not defined", e);
    }

    @Test
    public void testisPandigital() {
        boolean a = MathFunctions.isPandigital("1023457968");
        boolean b = MathFunctions.isPandigital("1023459768");
        boolean c = MathFunctions.isPandigital("1234567891");

        assertTrue("1023457968 is pandigital", a);
        assertTrue("1023459768 is pandigital", b);
        assertFalse("1234567891 is not pandigital", c);
    }

    @Test
    public void testisPermutation() {
        boolean a = MathFunctions.isPermutation(12, 21);
        boolean b = MathFunctions.isPermutation(123, 131);
        boolean c = MathFunctions.isPermutation(8749, 9847);
        boolean d = MathFunctions.isPermutation(0123, 123);

        assertTrue("12 and 21 are permutable", a);
        assertFalse("123 and 131 are not permutable", b);
        assertTrue("8749 and 9847 are permuteable", c);
        assertFalse("0123 and 123 are not permutable", d);
    }

    @Test
    public void testisPerfectNumber() {
        boolean a = MathFunctions.isPerfectNumber(8128);
        boolean b = MathFunctions.isPerfectNumber(1);
        boolean c = MathFunctions.isPerfectNumber(234567);
        assertTrue("8128 is perfect", a);
        assertFalse("1 is not perfect", b);
        assertFalse("234567 is not perfect", c);
    }

    @Test
    public void testareaOfTriangle() {
        int num = MathFunctions.areaOfTriangle(1,2, 2,3, 3,0);
        int num2 = MathFunctions.areaOfTriangle(13,17, 63,3, 7,47);

        assertEquals(2, num);
        assertEquals(708, num2);
    }

    @Test
    public void testtriangleContainsPoint() {
        boolean a = MathFunctions.triangleContainsPoint(13,17, 63,3, 7,47, 25,25);
        boolean b = MathFunctions.triangleContainsPoint(13,17, 63,3, 7,47, 10,10);

        assertTrue(a);
        assertFalse(b);
    }

    @Test
    public void testreverseInt() {
        int a = MathFunctions.reverseInt(0);
        int b = MathFunctions.reverseInt(2);
        int c = MathFunctions.reverseInt(123456789);

        assertEquals(0, a);
        assertEquals(2, b);
        assertEquals(987654321, c);
    }

    @Test
    public void testkelvinToCelsius() {
        double a = MathFunctions.kelvinToCelsius(1578);

        assertEquals(1304.85d, a, DELTA);
    }

    @Test
    public void testcongruentModulo() {
        boolean a = MathFunctions.congruentModulo(38, 14, 12);

        assertTrue(a);
    }

    @Test
    public void testcomplexAbs() {
        double num = MathFunctions.complexAbs(new double[]{3, 4});

        assertEquals(5.0d, num, DELTA);
    }

    @Test
    public void testphi() {
        int num = MathFunctions.phi(13);
        int num2 = MathFunctions.phi(99);

        assertEquals(12, num);
        assertEquals(60, num2);
    }

    @Test
    public void testvectorAdd() throws Exception {
        Vector a = MathFunctions.vectorAdd(new Vector(1, 2, 3), new Vector(4, 5, 6));

        assertArrayEquals(new double[]{5, 7, 9}, a.getArray(), DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorAddException() throws Exception {
        Vector a = MathFunctions.vectorAdd(new Vector(1, 2), new Vector(1, 2, 3));
    }
        
    @Test
    public void testvectorSub() throws Exception {
        Vector a = MathFunctions.vectorSub(new Vector(4, 5, 6), new Vector(1, 2, 3));

        assertArrayEquals(new double[]{3, 3, 3}, a.getArray(), DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorSubException() throws Exception {
        Vector a = MathFunctions.vectorSub(new Vector(1, 2), new Vector(1, 2, 3));
    }

    @Test
    public void testvectorDot() throws Exception {
        double n = MathFunctions.vectorDot(new Vector(1, 2, 3), new Vector(3, 4, 5));

        assertEquals(26.0d, n, DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorDotException() throws Exception {
        double n = MathFunctions.vectorDot(new Vector(1, 2), new Vector(1, 2, 3));
    }

    @Test
    public void testvectorCross() throws Exception {
        Vector a = MathFunctions.vectorCross(new Vector(1, 2, 3), new Vector(4, 5, 6));

        assertArrayEquals(new double[]{-3, 6, -3}, a.getArray(), DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorCrossException() throws Exception {
        Vector a = MathFunctions.vectorCross(new Vector(1, 2), new Vector(1, 2, 3, 4));
    }

    @Test
    public void testvectorTripleProduct() throws Exception {
        double a = MathFunctions.vectorTripleProduct(new Vector(1, 2, 3), new Vector(4, 5, 6), new Vector(3, 4, 5));

        assertEquals(0.0d, a, DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorTripleProductException() throws Exception {
        double a = MathFunctions.vectorTripleProduct(new Vector(1, 2), new Vector(2, 3), new Vector(1, 2, 3));
    }

    @Test
    public void testvectorLen() throws Exception {
        double a = MathFunctions.vectorLen(new Vector(3, 4));

        assertEquals(5.0, a, DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorLenException() throws Exception {
        double a = MathFunctions.vectorLen(new Vector(1));
    }

    @Test
    public void testvectorAngle() throws Exception {
        double a = MathFunctions.vectorAngle(new Vector(2, 9, 5), new Vector(6, -2, 4));

        assertEquals(79.72478922d, a, DELTA);
    }

    @Test(expected = Exception.class)
    public void testvectorAngleException() throws Exception {
        double a = MathFunctions.vectorAngle(new Vector(1, 2), new Vector(1, 2, 3));
    }

    @Test
    public void testmatrixAdd() throws Exception {
        Matrix a = MathFunctions.matrixAdd(new Matrix(new double[][]{{1,2,3},{4,5,6},{7,8,9}}), new Matrix(new double[][]{{9,8,7},{6,5,4},{3,2,1}}));

        assertTrue(Arrays.deepEquals(new double[][]{{10,10,10},{10,10,10},{10,10,10}}, a.getArray()));
    }

    @Test(expected = Exception.class)
    public void testmatrixAddException() throws Exception {
        Matrix a = MathFunctions.matrixAdd(new Matrix(new double[][]{{1,2},{1,2},{1,2}}), new Matrix(new double[][]{{1},{1},{1}}));
    }

    @Test
    public void testmatrixScalarMul() {
        Matrix a = MathFunctions.matrixScalarMul(2, new Matrix(new double[][]{{1,2},{3,4}}));

        assertTrue(Arrays.deepEquals(new double[][]{{2,4},{6,8}}, a.getArray()));
    }

    @Test
    public void testmatrixMul() throws Exception {
        Matrix a = MathFunctions.matrixMul(new Matrix(new double[][]{{3,2,1},{1,0,2}}), new Matrix(new double[][]{{1,2},{0,1},{4,0}}));

        assertTrue(Arrays.deepEquals(new double[][]{{7,8},{9,2}}, a.getArray()));
    }

    @Test(expected = Exception.class)
    public void testmatrixMulException() throws Exception {
        Matrix a = MathFunctions.matrixMul(new Matrix(new double[][]{{1,2},{2,1}}), new Matrix(new double[][]{{1,2,3},{3,2,1}}));
    }

    @Test
    public void testmatrixVectorMul() throws Exception {
        Vector a = MathFunctions.matrixVectorMul(new Matrix(new double[][]{{3,2,1},{1,0,2}}), new Vector(1,0,4));

        assertArrayEquals(new double[]{7,9}, a.getArray(), DELTA);
    }

    @Test(expected = Exception.class)
    public void testmatrixVectorMulException() throws Exception {
        Vector a = MathFunctions.matrixVectorMul(new Matrix(new double[][]{{3,2,1},{1,0,2}}), new Vector(1,0));
    }

    @Test
    public void testtransposeMatrix() {
        Matrix a = MathFunctions.transposeMatrix(new Matrix(new double[][]{{1,4},{8,-2},{-3,5}}));

        assertTrue(Arrays.deepEquals(new double[][]{{1,8,-3},{4,-2,5}}, a.getArray()));
    }

    @Test
    public void testisSymmetricMatrix() {
        boolean a = MathFunctions.isSymmetricMatrix(new Matrix(new double[][]{{1,3,0},{3,2,6},{0,6,5}}));
        boolean b = MathFunctions.isSymmetricMatrix(new Matrix(new double[][]{{1,3},{5,7}}));

        assertTrue(a);
        assertFalse(b);
    }

    @Test
    public void testsolveEquationSystem() {
        double[] res = MathFunctions.solveEquationSystem(new Matrix(new double[][]{{2,3,5,8},{1,1,-2,7},{3,-1,1,2}}));

        assertArrayEquals(new double[]{2,3,-1}, res, DELTA);
    }    

    @Test
    public void testmatrixDet() {
        double res = MathFunctions.matrixDet(new Matrix(new double[][]{{1,2},{3,4}}));
        double res2 = MathFunctions.matrixDet(new Matrix(new double[][]{{3,-2},{6,-4}}));

        double res3 = MathFunctions.matrixDet(new Matrix(new double[][]{{2,5,2},{3,-3,1},{1,4,-4}}));
        double res4 = MathFunctions.matrixDet(new Matrix(new double[][]{{1,2,3},{4,5,6},{7,8,9}}));

        double res5 = MathFunctions.matrixDet(new Matrix(new double[][]{{1,4,3,2},{2,1,4,3},{3,2,1,4},{4,3,2,1}}));

        assertEquals(-2.0d, res, DELTA);
        assertEquals(0.0d, res2, DELTA);

        assertEquals(111.0d, res3, DELTA);
        assertEquals(0.0d, res4, DELTA);

        assertEquals(-160.0d, res5, DELTA);
    }
}
