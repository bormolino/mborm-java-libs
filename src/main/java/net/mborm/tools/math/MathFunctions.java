package net.mborm.tools.math;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Math-class for extended math-functions
 *
 * @author Maximilian Borm
 * @version 1.9-stable
 **/

public class MathFunctions {

    /**
     * Converting integer to binary number
     *
     * @param   digit   integer number to be converted
     * @return binary String
     */
    public static String intToBinary(int digit) {
        return Integer.toBinaryString(digit);
    }

    /**
     * Converting binary String to integer
     *
     * @param   bin     binary String to be converted
     * @return int number
     */
    public static int binaryToInt(String bin) {
        return Integer.parseInt(bin, 2);
    }

    /**
     * Converting integer to hexadecimal number
     *
     * @param   dez     integer number to be converted
     * @return hex String
     */
    public static String intToHex(int dez) {
        return Integer.toHexString(dez);
    }

    /**
     * Converting hexadecimal String to integer
     *
     * @param   hex     String to be converted
     * @return int number
     */
    public static int hexToInt(String hex) {
        return Integer.parseInt(hex, 16);
    }

    /**
     * Getting the date of easter for every given year
     *
     * @param   year    the year
     * @return int array with: [0]=day,[1]=month,[2]=year
     */
    public static int[] getEasternDate(int year) {
        int a = year % 19;
        int b = year / 100;
        int c = year % 100;
        int d = (a * 19 + b + 15 - (b / 4) - ((b - ((b + 8) / 25) + 1) / 3)) % 30;
        int e = (((b % 4) + (c / 4)) * 2 + 32 - d - (c % 4)) % 7;
        int f = (((e * 2 + d) * 11 + a) / 451);

        return new int[]{((d + e + 114 - (f * 7)) % 31) + 1, ((d + e + 114 - (f * 7)) / 31), year};
    }

    /**
     * Method to calculate the binomial coefficient
     *
     * @param   n   n-value
     * @param   k   k-value
     * @return double result
     */
    public static double binomial(double n, double k) {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }

    /**
     * Method to calculate the factorial recursive
     *
     * @param   n   n-value
     * @return double result
     */
    public static double recFactorial(double n) {
        return (n == 1 || n == 0) ? 1 : recFactorial(n - 1) * n;
    }

    /**
     * Method to calculate the factorial
     *
     * @param   n   n-value
     * @return double result
     */
    public static double factorial(double n) {
        if(n == 0)
            return 1;
        else {
            double f;
            for(f = n; n > 1; f *= --n);
            return f;
        }
    }

    /**
     * Get digit sum of an given integer
     *
     * @param   n   integer value
     * @return digit sum of param n
     */
    public static int getDigitSum(int n) {
        int sum = 0;

        while(n > 0) {
            sum += n % 10;
            n /= 10;
        }

        return sum;
    }

    /**
     * Get digit sum of big integer numbers
     *
     * @param   n   BigInteger number
     * @return BigInteger sum
     * @see java.math.BigInteger
     */
    public static BigInteger getDigitSum(BigInteger n) {
        BigInteger sum = BigInteger.ZERO;

        while(n.compareTo(BigInteger.ZERO) == 1) {
            sum = sum.add(n.mod(new BigInteger("10")));
            n = n.divide(new BigInteger("10"));
        }

        return sum;
    }

    /**
     * Calculating brutto and netto values
     *
     * @param   hourlyWage     String wage per hour
     * @param   hours          String worked hours
     * @param   deductions     String deductions like fees or something
     * @return double array with {brutto, netto}
     */
    public static double[] getBruttoNetto(double hourlyWage, double hours, double deductions) {
        double brutto = hourlyWage * hours;
        return new double[]{brutto, brutto * (100.0 - deductions) / 100.0};
    }


    /**
     * Calculating the compound interest
     *
     * @param   asset       sum to be compound
     * @param   rate        rate in percent
     * @param   runtime     runtime in years
     * @return end value of the asset
     */
    public static double compoundInterest(double asset, double rate, double runtime) {
        for(int i = 0; i < runtime; i++)
            asset += (rate / 100) * asset;

        return asset;
    }

    /**
     * Greatest common devisor
     *
     * @param   a   a-value
     * @param   b   b-value
     * @return greatest divisor found
     */
    public static int gcd(int a, int b) {
        return (b == 0) ? Math.abs(a) : Math.abs(gcd(b, a % b));
    }

    /**
     * Lowest common multiple
     *
     * @param   a   a-value
     * @param   b   b-value
     * @return lowest common multiple
     */
    public static int lcm(int a, int b) {
        return a * b / gcd(a, b);
    }

    /**
     * Checks if a given number is pentagonal
     *
     * @param   n   number to be checked
     * @return true or false
     */
    public static boolean isPentagonal(int n) {
        double x = (Math.sqrt(24 * n + 1) + 1) / 6;
        return (x == (int) x);
    }

    /**
     * Enhanced isPrime function, which checks if a givne number is a prime
     *
     * @param   n   number to be checked
     * @return true or false
     */
    public static boolean isPrime(int n) {
        if(n == 1)
            return false;

        else if(n == 2)
            return true;

        else {

            int i, r = (int) Math.floor(Math.sqrt(n));

            for(i = 2; i <= r && n % i != 0; ++i);

            return (r == i - 1);
        }
    }

    /**
     * Calculates primesum of a given number
     *
     * @param   n   max number
     * @return prime sum
     */
    public static int primeSum(int n) {
        int sum = 0;

        for(int i = n; i != 0; i--)
            if(isPrime(i))
                sum += i;

        return sum;
    }

    /**
     * Checks if a given number is circular prime number
     *
     * @param   n   number to be checked
     * @return true or false
     */
    public static boolean isCircularPrime(int n) {
        String s = Integer.toString(n);

        for(int i = 0; i < s.length(); i++)
            if(!isPrime((Integer.parseInt(s.substring(i) + s.substring(0, i)))))
                return false;

        return true;
    }

    /**
     * Checks if a given number is pandigital
     *
     * @param   n   number to be checked
     * @return true or false
     */
    public static boolean isPandigital(String n) {
        final char[] c = n.toCharArray();
        Arrays.sort(c);

        return Arrays.equals(c, "0123456789".toCharArray());
    }

    /**
     * Checks if a given number is a permutation of another given number
     *
     * @param   a   number to be checked 
     * @param   b    number to check
     * @return true or false
     */
    public static boolean isPermutation(int a, int b) {
        if(String.valueOf(a).length() != String.valueOf(b).length())
            return false;
        else {
            int[] digits = new int[10];

            for(int i = 0; i < 10; i++)
                digits[i] = 0;

            while(a != 0) {
                digits[a % 10]++;
                a /= 10;
            }

            while(b != 0) {
                digits[b % 10]--;
                b /= 10;
            }

            for(int i = 0; i < 10; i++)
                if(digits[i] != 0)
                    return false;

            return true;
        }
    }

    /**
     * Checks if a given number is perfect
     *
     * @param   n   number to be checked
     * @return true or false
     */
    public static boolean isPerfectNumber(int n) {
        if(n == 1)
            return false;
        else {
            int tot = 1, max = (int) Math.sqrt(n) + 1;

            for(int i = 2; i < max; i++)
                if((n % i) == 0) {
                    tot += i;
                    int q = n / i;

                    if(q > i)
                        tot += q;
                }

            return (tot == n);
        }
    }

    /**
     * Calculates the area of a triangle (Gaußsche Dreiecksformel)
     *
     * @param   ax   ax-value
     * @param   ay   ay-value
     * @param   bx   bx-value
     * @param   by   by-value
     * @param   cx   cx-value
     * @param   cy   cy-value
     * @return area of given triangle
     */
    public static int areaOfTriangle(int ax, int ay, int bx, int by, int cx, int cy) {
        return Math.abs((ax - cx) * (by - ay) - (ax - bx) * (cy - ay)) / 2;
    }

    /**
     * Method to check if a triangle contains a given point
     *
     * @param   ax   ax-value
     * @param   ay   ay-value
     * @param   bx   bx-value
     * @param   by   by-value
     * @param   cx   cx-value
     * @param   cy   cy-value
     * @param   px   px-value of point
     * @param   py   py-value of point
     * @return true or false
     */
    public static boolean triangleContainsPoint(int ax, int ay, int bx, int by, int cx, int cy, int px, int py) {
        return (areaOfTriangle(ax, ay, bx, by, cx, cy) ==
                areaOfTriangle(ax, ay, bx, by, px, py) +
                areaOfTriangle(ax, ay, px, py, cx, cy) +
                areaOfTriangle(px, py, bx, by, cx, cy));
    }

    /**
     * Reversing an int number
     *
     * @param   n   number to be reversed
     * @return reversed number
     */
    public static int reverseInt(int n) {
        int r = 0;

        while(n != 0) {
            r *= 10;
            r += n % 10;
            n /= 10;
        }

        return r;
    }

    /**
     * Converting kelvin to celsius
     *
     * @param   d   kelvin value
     * @return celsius value
     */
    public static double kelvinToCelsius(double d) {
        return d - 273.15d;
    }

    /**
     * Calculates the congruent modulo
     *
     * @param a     a-value
     * @param b     b-value
     * @param c     c-value
     * @return true or false
     */
    public static boolean congruentModulo(int a, int b, int c) {
        if(c == 0)
            return false;

        return a % c == b % c;
    }

    /**
     * Calculate absolute number of an complex number
     *
     * @param   z    z[0] real part, z[1] imaginary part
     * @return double value of complex number
     */
    public static double complexAbs(double[] z) {
        return Math.sqrt(z[0] * z[0] + z[1] * z[1]);
    }

    /**
     * Calculates eulers totient function phi
     *
     * @param    n     number to calculate
     * @return coprime integers smaller n
     */
    public static int phi(int n) {
        int c = 0;

        for(int i = 1; i < n; i += (n % 2 < 1) ? 2 : 1)
            if(gcd(n, i) == 1)
                c++;

        return c;
    }

    /**
     * Method to add two vectors
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @return Vector object
     * @throws Exception e
     */
    public static Vector vectorAdd(Vector v1, Vector v2) throws Exception {
        if(v1.getDim() != v2.getDim())
            throw new Exception("MATH-ERR: Vectors do not have the same length!");
        else {
            Vector res = new Vector();
            res.components = new double[v1.getDim()];

            for(int i = 0; i < v1.getDim(); i++)
                res.components[i] = v1.components[i] + v2.components[i];

            return res.getVector();
        }
    }

    /**
     * Method to substract two vectors
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @return Vector object
     * @throws Exception e
     */
    public static Vector vectorSub(Vector v1, Vector v2) throws Exception {
        if(v1.getDim() != v2.getDim())
            throw new Exception("MATH-ERR: Vectors do not have the same length!");
        else {
            Vector res = new Vector();
            res.components = new double[v1.getDim()];

            for(int i = 0; i < v1.getDim(); i++)
                res.components[i] = v1.components[i] - v2.components[i];

            return res;
        }
    }


    /**
     * Method to calculate the vector dot (Vektorprodukt)
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @return result
     * @throws Exception e
     */
    public static double vectorDot(Vector v1, Vector v2) throws Exception {
        if(v1.getDim() != v2.getDim())
            throw new Exception("MATH-ERR: Vectors do not have the same length!");
        else {
            double sum = 0;

            for(int i = 0; i < v1.getDim(); i++)
                sum += v1.components[i] * v2.components[i];

            return sum;
        }
    }

    /**
     * Method to calculate the cross product (Kreuzprodukt)
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @return Vector object
     * @throws Exception e
     */
    public static Vector vectorCross(Vector v1, Vector v2) throws Exception {
        if(v1.getDim() != 3 || v2.getDim() != 3)
            throw new Exception("MATH-ERR: vector cross is only defined for R^3!");
        else {
            Vector res = new Vector();
            res.components = new double[3];

            res.components[0] = v1.components[1] * v2.components[2] - v1.components[2] * v2.components[1];
            res.components[1] = v1.components[2] * v2.components[0] - v1.components[0] * v2.components[2];
            res.components[2] = v1.components[0] * v2.components[1] - v1.components[1] * v2.components[0];

            return res;
        }
    }

    /**
     * Method to calculate the vector triple product (Spatprodukt)
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @param v3 vector object
     * @return double tripleProduct
     * @throws Exception e
     */
    public static double vectorTripleProduct(Vector v1, Vector v2, Vector v3) throws Exception {
        if(v1.getDim() != 3 || v2.getDim() != 3 || v3.getDim() != 3)
            throw new Exception("MATH-ERR: vector triple product is only defined for R^3!");
        else 
            return vectorDot(vectorCross(v1, v2), v3);
    }

    /**
     * Calulates the length of an vector
     *
     * @param v1 Vector object
     * @return length ob vector
     * @throws Exception e
     */
    public static double vectorLen(Vector v1) throws Exception {
        if(v1.getDim() < 2)
            throw new Exception("MATH-ERR: Vector must have at least two components to calculate the length!");
        else {
            double tmp = 0;

            for(int i = 0; i < v1.getDim(); i++)
                tmp += v1.components[i] * v1.components[i];

            return Math.sqrt(tmp);
        }
    }

    /**
     * Calculates the angle between two vectors
     *
     * @param v1 Vector object
     * @param v2 Vector object
     * @return double angle
     * @throws Exception e
     */
    public static double vectorAngle(Vector v1, Vector v2) throws Exception {
        try {
            return Math.atan(vectorLen(vectorCross(v1, v2)) / vectorDot(v1, v2)) * 180.0 / Math.PI;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * Add two matrices
     *
     * @param m1 Matrix object
     * @param m2 Matrix object
     * @return Matrix object
     * @throws Exception e
     */
    public static Matrix matrixAdd(Matrix m1, Matrix m2) throws Exception {
        if(!Arrays.equals(m1.getDim(), m2.getDim()))
            throw new Exception("MATH-ERR: Matrices does not have the same format for addition");
        else {
            final int[] dim = m1.getDim();
            Matrix res = new Matrix(new double[dim[0]][dim[1]]);

            for(int i = 0; i < dim[0]; i++)
                for(int j = 0; j < dim[1]; j++)
                    res.components[i][j] = m1.components[i][j] + m2.components[i][j];

            return res;
        }
    }

    /**
     * scalar multiplication with a matrix
     *
     * @param n double scalar
     * @param m Matrix object
     * @return Matrix object
     */
    public static Matrix matrixScalarMul(double n, Matrix m) {
        final int dim[] = m.getDim();
        Matrix res = new Matrix(new double[dim[0]][dim[1]]);

        for(int i = 0; i < dim[0]; i++)
            for(int j = 0; j < dim[1]; j++)
                res.components[i][j] = n * m.components[i][j];

        return res;
    }

    /**
     * matrix multiplication
     *
     * @param m1 Matrix object
     * @param m2 Matrix object
     * @return Matrix object
     * @throws Exception e
     */
    public static Matrix matrixMul(Matrix m1, Matrix m2) throws Exception {
        final int m1Dim[] = m1.getDim();
        final int m2Dim[] = m2.getDim();

        if(m1Dim[0] != m2Dim[1])
            throw new Exception("MATH-ERR: MatrixMul not defined for "+m1Dim[0]+" x "+m2Dim[1]+" matrices!");
        else {
            Matrix res = new Matrix(new double[m1Dim[0]][m2Dim[1]]);

            for(int i = 0; i < m1Dim[0]; i++)
                for(int k = 0; k < m2Dim[1]; k++)
                    for(int j = 0; j < m1Dim[1]; j++)
                        res.components[i][k] += m1.components[i][j] * m2.components[j][k];

            return res;
        }
    }

    /**
     * matrix vector multiplication
     *
     * @param m Matrix object
     * @param v Vector object
     * @return Vector object
     * @throws Exception e
     */
    public static Vector matrixVectorMul(Matrix m, Vector v) throws Exception {
        final int mDim[] = m.getDim();

        if(mDim[1] != v.getDim())
            throw new Exception("MATH-ERR: MatrixVectorMul not defined for different dimensions!");
        else {
            Vector res = new Vector();
            res.components = new double[mDim[0]];

            for(int i = 0; i < mDim[0]; i++)
                for(int j = 0; j < mDim[1]; j++)
                    res.components[i] += m.components[i][j] * v.components[j];

            return res;
        }
    }

    /**
     * transpose matrix
     *
     * @param m Matrix object
     * @return Matrix object
     */
    public static Matrix transposeMatrix(Matrix m) {
        final int mDim[] = m.getDim();
        Matrix res = new Matrix(new double[mDim[1]][mDim[0]]);

        for(int i = 0; i < mDim[0]; i++)
            for(int j = 0; j < mDim[1]; j++)
                res.components[j][i] = m.components[i][j];

        return res;
    }

    /**
     * isSymmetricMatrix
     *
     * @param m Matrix object
     * @return boolean
     */
    public static boolean isSymmetricMatrix(Matrix m) {
        return Arrays.deepEquals(m.components, (transposeMatrix(m)).components); //equals always returns false for multi-dim-arrays
    }

    /**
     * solveEquationSystem - solve linear equation system with the gauss elimination method
     *
     * @param m Matrix object
     * @return double array with n solutions
     */
    public static double[] solveEquationSystem(Matrix m) {
        final int l[] = m.getDim();
        int i, j, k, n = l[0];
        double c, sum = 0.0d;
        double[] x = new double[n];

        for(j = 0; j < n; j++) {
            for(i = 0; i < n; i++) {
                if(i > j) {
                    c = m.components[i][j] / m.components[j][j];

                    for(k = 0; k < n + 1; k++) {
                        m.components[i][k] = m.components[i][k] - c * m.components[j][k];
                    }
                }
            }
        }

        x[n-1] = m.components[n-1][n] / m.components[n-1][n-1];

        for(i = n - 2; i >= 0; i--) {
            sum = 0.0d;

            for(j = i + 1; j < n; j++) {
                sum += m.components[i][j] * x[j];
            }

            x[i] = (m.components[i][n] - sum) / m.components[i][i];
        }

        return x;
    }

    private static double[][] lowerMatrix(double[][] m, int x, int y) {
        int l = m.length - 1;
        double[][] res = new double[l][l];

        for(int i = 0; i < l; i++) {
            for(int j = 0; j < l; j++) {
                if(i < x && j < y)
                    res[i][j] = m[i][j];
                else if(i >= x && j < y)
                    res[i][j] = m[i+1][j];
                else if(i < x && j >= y)
                    res[i][j] = m[i][j+1];
                else
                    res[i][j] = m[i+1][j+1];
            }
        }

        return res;
    }

    /**
     * matrixDet - calculate determinant of a matrix
     *
     * @param m Matrix object
     * @return double value of determinant
     */
    public static double matrixDet(Matrix m) {
        final int l[] = m.getDim();

        if(l[0] == 1 && l[1] == 1) {
            return m.components[0][0];
        }

        if(l[0] == 2 && l[1] == 2) {
            return (m.components[0][0]*m.components[1][1] - m.components[0][1]*m.components[1][0]);
        }

        //Rule of Sarrus
        if(l[0] == 3 && l[1] == 3) {
            return (m.components[0][0]*m.components[1][1]*m.components[2][2]+
                    m.components[0][1]*m.components[1][2]*m.components[2][0]+
                    m.components[0][2]*m.components[1][0]*m.components[2][1]-
                    m.components[0][2]*m.components[1][1]*m.components[2][0]-
                    m.components[0][0]*m.components[1][2]*m.components[2][1]-
                    m.components[0][1]*m.components[1][0]*m.components[2][2]);
        }

        //laplace's formula
        else {
            int signum = 1;
            double sum = 0;

            for(int i = 0; i < l[0]; i++) {
                sum += signum * m.components[0][i] * matrixDet(new Matrix(lowerMatrix(m.getArray(), 0, i)));
                signum *= -1;
            }

            return sum;
        }
    }
           
}
