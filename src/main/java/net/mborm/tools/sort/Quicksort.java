package net.mborm.tools.sort;

/**
 * Quicksort algorithm in java with generic types for example Integer, Character, Float ...
 * TODO: fix String sort
 *
 * @author Maximilian Borm
 * @version 1.1-testing
 **/

@SuppressWarnings("unchecked")
public class Quicksort<T extends Comparable<? super T>> {

    private T[] s_ = null;

    /**
     * Sort method, takes array to be sorted an returns sorted array
     *
     * @param array array to be sorted
     * @return sorted array
     */
    public T[] sort(T... array) {
        this.s_ = array;
        this.quickSort(0, this.s_.length - 1);

        return this.s_;
    }

    private void quickSort(int left, int right) {
        if (right <= left)
            return;

        else {
            int pivot = this.partition(left, right);

            this.quickSort(left, pivot - 1);
            this.quickSort(pivot + 1, right);
        }
    }

    private int partition(int left, int right) {
        int pivot = right, i = left, j = right - 1;

        while (i < j) {
            while (this.s_[i].compareTo(this.s_[pivot]) <= 0 && i < right)
                ++i;

            while (this.s_[i].compareTo(this.s_[pivot]) > 0 && j > left)
                --j;

            if (i < j)
                this.swap(i, j);
        }

        if (this.s_[i].compareTo(this.s_[pivot]) > 0) {
            this.swap(i, pivot);
            pivot = i;
        }

        return pivot;
    }

    private void swap(int i, int j) {
        T a = this.s_[i];
        this.s_[i] = this.s_[j];
        this.s_[j] = a;
    }
}
