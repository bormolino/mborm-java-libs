package net.mborm.tests.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import net.mborm.tools.math.Vector;

/**
  * Tests for Vector class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/

public class VectorTest {
    public static final double DELTA = 0.00000001d;

    @Test
    public void testConstructor() {
        Vector a = new Vector(1,2,3,4);

        assertArrayEquals(new double[]{1,2,3,4}, a.getArray(), DELTA);
    }

    @Test
    public void testgetVector() {
        Vector a = new Vector(1,2,3);

        assertTrue(a == a.getVector());
    }

    @Test
    public void testgetDim() {
        Vector a = new Vector(1,2,3);

        assertEquals(3, a.getDim());
    }
        
}
