package net.mborm.tests.sort;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

import net.mborm.tools.sort.Quicksort;

/**
  * Tests for Quicksort class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/


public class QuicksortTest {
    public static final double DELTA = 0.00000001d;

    @Test
    public void sortIntegerTest() {
        Integer[] iarr = new Integer[100];
        int cnt = -1;

        for(int i = 100; i > 0; i--)
            iarr[++cnt] = Integer.valueOf(i);

        Quicksort<Integer> m = new Quicksort<Integer>();
        Integer[] sorted = m.sort(iarr);

        for(int i = 1; i <= 100; i++)
            assertEquals(i, sorted[i-1].intValue());
    }

    @Test
    public void sortDoubleTest() {
        Double[] darr = new Double[100];
        int cnt = -1;

        for(double i = 100; i > 0; i--)
            darr[++cnt] = Double.valueOf(i);

        Quicksort<Double> m = new Quicksort<Double>();
        Double[] sorted = m.sort(darr);

        for(int i = 1; i <= 100; i++)
            assertEquals(i, sorted[i-1].doubleValue(), DELTA);
    }
}
