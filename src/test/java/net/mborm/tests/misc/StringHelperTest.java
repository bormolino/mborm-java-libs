package net.mborm.tests.misc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import net.mborm.tools.misc.StringHelper;

/**
  * Tests for StringHelper class
  *
  * @author Maximilian Borm
  * @version 1.0-stable
  **/

public class StringHelperTest {
    @Test
    public void isPalindromeTest() {
        boolean a = StringHelper.isPalindrome("reliefpfeiler");
        boolean b = StringHelper.isPalindrome("1335331");
        boolean c = StringHelper.isPalindrome("test");

        assertTrue(a);
        assertTrue(b);
        assertFalse(c);
    }

    @Test
    public void isMatchTest() {
        boolean a = StringHelper.isMatch("1234", "\\d{4}");
        boolean b = StringHelper.isMatch("Das ist ein Test", ".*ist.*");
        boolean c = StringHelper.isMatch("Hallo", "\\s{1}");
        boolean d = StringHelper.isMatch("Abc", ".*[a-zA-Z]{2,}.*");
        boolean e = StringHelper.isMatch("https://mborm.net/test/", "^https?://[^/]+/.*/");

        assertTrue(a);
        assertTrue(b);
        assertFalse(c);
        assertTrue(d);
        assertTrue(e);
    }
}
