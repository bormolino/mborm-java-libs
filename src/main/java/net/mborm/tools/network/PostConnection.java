package net.mborm.tools.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Class to send POST requests to server and read answer as plain text/code<br><br>
 * 
 * {@code
 * Usage:
 * PostConnection con = new PostConnection("https://mborm.net/POST.php");
 * con.request("param", "value", "paramtwo", "anothercontent");
 * String response = con.response();
 * }
 *
 * @author Maximilian Borm
 * @version 1.2-stable
 **/

public final class PostConnection {

    private String _address, _output;
    private URLConnection _conn;

    /**
     * Constructor which takes the address of the server
     *
     * @param address - url like https://mborm.net/POST.php
     */
    public PostConnection(String address) {
        this._address = address;
    }


    /**
     * Transfer POST-Request key-value pairs
     *
     * @param keys - key-value pairs like: request("key", "value", "num", "3");
     */
    public void request(String... keys) {
        String format = "";

        for(int i = 0; i < keys.length; i += 2)
            format += String.format("%s=%s&", keys[i], keys[i + 1]);

        try {
            URL url = new URL(this._address);
            this._conn = url.openConnection();
            this._conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(this._conn.getOutputStream());
            wr.write(format);
            wr.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the (optional) response of the server - may cause NullPointerException if value is unchecked before usage
     *
     * @return - Server answer or null
     */
    public String response() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this._conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null)
                sb.append(line + "\n");

            this._output = sb.toString();
            reader.close();

            return this._output;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
