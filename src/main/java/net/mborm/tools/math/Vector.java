package net.mborm.tools.math;

import java.util.Arrays;

/**
 * Class for structured vectors and for vector function usage
 *
 * @author Maximilian Borm
 * @version 1.3-stable
 **/

public class Vector {

    /**
     * components of the vector
     */
    protected double[] components;

    public Vector() {
    }

    /**
     * Constructor for an vector with n components
     * @param vars double-varargs for components
     */
    public Vector(double... vars) {
        this.components = vars;
    }

    /**
     * Get the vector object
     * @return this
     */
    public Vector getVector() {
        return this;
    }

    /**
     * Time of components (dimension) of vector
     * @return int dim
     */
    public int getDim() {
        return this.components.length;
    }

    /**
      * Get array with vector values
      * @return double[] arr
      */
    public double[] getArray() {
        return this.components;
    }

    /**
     * {@inheritDoc}
     * Overrides the standard toString() function to structurize the vector output
     * @return structurized vector String
     */
    @Override
    public String toString() {
        return Arrays.toString(this.components);
    }

    /**
     * Prints the overwritten toString() function
     */
    public void printVector() {
        System.out.println(this.toString());
    }
}
